﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class FlashlightController : MonoBehaviour
{
    //bool lightOn = false;
    private Light myLight;
    public float sanity = 100f;
    public float sanityMult = 1f;
    public Slider flashlightSlider;
    public Image flashlightFill;
    public Image brainFill;
    public Slider sanitySlider;
    public float batteryDrainMult = 1f;
    // Start is called before the first frame update
    void Start()
    {
        myLight = GetComponent<Light>();
        //Scene scene = SceneManager.GetActiveScene()
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetButtonDown("Fire1"))
        {
            myLight.enabled = !myLight.enabled;

        }
        if (myLight.enabled)
        {
            //Debug.Log("Light is now decreasing in intensity");
            if (myLight.intensity > 0f)
            {
                myLight.intensity -= Time.deltaTime * (1f * batteryDrainMult);
                Debug.Log(flashlightSlider.value);
                flashlightSlider.value -= Time.deltaTime * (1f * batteryDrainMult);
                setSliderColor();
            }
            if (myLight.intensity <= 0f)
            {
                myLight.intensity = 0f;
                flashlightSlider.value = 0f;
                sanity -= Time.deltaTime * (10f * sanityMult);
                myLight.enabled = !myLight.enabled;
            }
        }
        if (!myLight.enabled)
        {
            //Debug.Log("Sanity is now decreasing");
            sanity -= Time.deltaTime * (10f * sanityMult);
            sanitySlider.value -= Time.deltaTime * (10f * sanityMult);
            setBrainSliderColor();
            if (sanity < 0)
            {
                //loss case
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        }

    }
    void setSliderColor()
    {
        flashlightFill.color = Color.Lerp(Color.red,Color.green,flashlightSlider.value/20f);
    }
    void setBrainSliderColor()
    {
        brainFill.color = Color.Lerp(Color.black, Color.red, sanitySlider.value / sanity);
    }
}
